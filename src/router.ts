import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import FirstConnection from './views/first-connection/FirstConnection.vue';

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: '/',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/first-time-setup',
      name: 'first-time-setup',
      component: FirstConnection
    }
  ]
})
